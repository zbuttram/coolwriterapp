import React, { useEffect } from "react";
import classNames from "classnames";
import produce from "immer";
import { set } from "lodash";
import { searchGallery } from "./imgur";

function SettingsModal({ isOpen, close, settings, updateSettings }) {
  function updateSetting(setting) {
    return event => {
      const value = event.target.value;
      updateSettings(
        produce(draft => {
          set(draft, setting, value);
        })
      );
    };
  }

  const subreddit = settings.imageSettings.subreddit;

  return (
    <div className={classNames("modal", { "is-active": isOpen })}>
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Settings</p>
          <button className="delete" aria-label="close" onClick={close} />
        </header>
        <section className="modal-card-body">
          <input
            value={subreddit}
            onChange={updateSetting("imageSettings.subreddit")}
          />
        </section>
        <footer className="modal-card-foot">
          {/* <button className="button is-success">Save changes</button>
          <button className="button">Cancel</button> */}
        </footer>
      </div>
    </div>
  );
}

export default SettingsModal;
