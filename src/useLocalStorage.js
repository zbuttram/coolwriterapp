import localforage from "localforage";
import { useState, useEffect } from "react";
import { identity } from "lodash";

function useLocalStorage(
  key,
  initialValue,
  { serialize = identity, deserialize = identity } = {}
) {
  const [item, setItem] = useState(null);

  useEffect(
    () =>
      localforage.getItem(key).then(value => {
        if (value === null) {
          setItem(initialValue);
        } else {
          setItem(deserialize(value));
        }
      }),
    []
  );

  useEffect(
    () => {
      if (item !== null) {
        localforage.setItem(key, serialize(item));
      }
    },
    [item]
  );

  return [item, setItem];
}

export default useLocalStorage;
