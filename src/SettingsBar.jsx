import React, { useRef } from "react";
import produce from "immer";
import { set, identity, debounce, isFinite } from "lodash";
import styled from "react-emotion";

const BarDiv = styled("div")`
  height: 10%;
`;

function SettingsBar({ settings, updateSettings }) {
  function updateSetting(setting, value) {
    updateSettings(
      produce(draft => {
        set(draft, setting, value);
      })
    );
  }

  const decouncedUpdateSubreddit = useRef(
    debounce(v => {
      updateSetting("imageSettings.subreddit", v);
    }, 500)
  ).current;

  const { imageSettings } = settings;

  return (
    <BarDiv className="level">
      <div className="level-item">I want</div>
      <div className="level-item">
        <Select>
          <option value="top">top</option>
          <option value="time">the newest</option>
        </Select>
      </div>
      <div className="level-item">
        <Select>
          <option value="NO_ANIMATED">pics</option>
          {/*<option value="ANIMATED">pics + gifs</option>*/}
        </Select>
      </div>
      <div className="level-item">from</div>
      <div className="level-item">
        <Select>
          <option value="day">today</option>
          <option value="month">this month</option>
          <option value="year">this year</option>
          <option value="all">all time</option>
        </Select>
      </div>
      <div className="level-item">on</div>
      <div className="level-item">
        r/&nbsp;
        <input
          className="input"
          type="text"
          defaultValue={imageSettings.subreddit}
          onChange={e => decouncedUpdateSubreddit(e.target.value)}
        />
      </div>
      <div className="level-item">every</div>
      <div className="level-item">
        <input
          className="input"
          type="text"
          value={settings.wordGoal}
          onChange={e =>
            updateSetting(
              "wordGoal",
              guardNum(e.target.value, settings.wordGoal)
            )
          }
        />
      </div>
      <div className="level-item">words</div>
    </BarDiv>
  );
}

export default SettingsBar;

function guardNum(potentialNum, guard) {
  if (!isFinite(Number(potentialNum))) {
    return guard;
  }
  return Number(potentialNum);
}

function Select({ children }) {
  return (
    <div className="select">
      <select>{children}</select>
    </div>
  );
}
