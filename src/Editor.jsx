import React, { useEffect, useRef } from "react";
import styled from "react-emotion";
import { Editor } from "draft-js";

const EditorWrapper = styled("div")`
  cursor: text;
  height: 90%;
  //border-style: solid;
  //border-color: gray;
  .DraftEditor-root {
    height: 95%;
  }
  .DraftEditor-editorContainer {
    height: 95%;
  }
  .public-DraftEditor-content {
    height: 95%;
  }
  overflow-y: scroll;
`;

function CoolEditor(props) {
  const editor = useRef(null);

  useEffect(() => {
    setTimeout(editor.current.focus());
  }, []);

  return (
    <EditorWrapper>
      <Editor
        className={"textarea"}
        ref={editor}
        spellCheck={true}
        {...props}
      />
    </EditorWrapper>
  );
}

export default CoolEditor;
