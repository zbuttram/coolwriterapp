import React from "react";
import classNames from "classnames";

function Modal({ children, isOpen, title, close }) {
  return (
    <div className={classNames("modal", { "is-active": isOpen })}>
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" aria-label="close" onClick={close} />
        </header>
        <section className="modal-card-body">{children}</section>
        <footer className="modal-card-foot">
          {/* <button className="button is-success">Save changes</button>
          <button className="button">Cancel</button> */}
        </footer>
      </div>
    </div>
  );
}

export default Modal;
