import React, { useState, useEffect, useRef } from "react";
import { EditorState, RichUtils, convertFromRaw, convertToRaw } from "draft-js";
import Editor from "./Editor";
import styled from "react-emotion";
import { findImage } from "./finder";
import SettingsModal from "./SettingsModal";
import useLocalStorage from "./useLocalStorage";
import SettingsBar from "./SettingsBar";
import { isEqual } from "lodash";
import AboutModal from "./AboutModal";
import SplitterLayout from "react-splitter-layout";

const Section = styled("section")`
  height: 100%;
`;

const Container = styled("div")`
  height: 100%;
  padding-top: 10px;
`;

const MainDiv = styled("div")`
  height: 90%;
`;

const SectionBar = styled("div")`
  height: 5%;
`;

const SplitView = styled("div")`
  padding: 5px;
  height: 100%;
`;

const Splitter = styled(({ className, ...props }) => (
  <SplitterLayout customClassName={className} {...props} />
))`
  &&& {
    height: 100%;
    position: inherit;
    .layout-pane {
      height: 100%;
      position: inherit;
    }
  }
`;

function App() {
  const {
    editorState,
    updateEditorState,
    handleKeyCommand,
    wordCount
  } = useEditorState();
  const [image, setImage] = useState(null);
  const [lastWordGoal, setLastWordGoal] = useState(0);
  const [settingsModalOpen, setSettingsModalOpen] = useState(false);
  const [aboutModalOpen, setAboutModalOpen] = useState(false);
  const [findingImage, setFindingImage] = useState(false);
  const [settings, updateSettings] = useLocalStorage("writer-main-settings", {
    imageSettings: {
      subreddit: "cats",
      sort: "top",
      sortWindow: "week"
    },
    wordGoal: 10
  });
  const [secondaryPaneSize, setSecondaryPaneSize] = useLocalStorage(
    "writer-pane-size",
    undefined
  );

  if (settings === null || editorState === null || secondaryPaneSize === null) {
    return null;
  }

  const imageUrl = image ? image.link : null;
  const imageData = image ? JSON.stringify(image, null, 2) : null;
  const { imageSettings, wordGoal } = settings;
  const lastImageSettings = useRef(null);

  function getNewImage(imageSettings) {
    const sameImageSettings = () =>
      isEqual(imageSettings, lastImageSettings.current);
    if (findingImage && sameImageSettings()) {
      return;
    }
    lastImageSettings.current = imageSettings;
    setFindingImage(true);
    findImage(imageSettings)
      .then(img => {
        if (sameImageSettings()) {
          setImage(img);
        }
        setFindingImage(false);
      })
      .catch(() => {
        setFindingImage(false);
        if (sameImageSettings()) {
          alert("Failure in imgur api.");
        }
      });
  }

  // initial image fetch
  useEffect(
    () => {
      getNewImage(imageSettings);
    },
    [imageSettings]
  );

  const nextWordGoal = lastWordGoal + wordGoal;

  // react to word count updates
  useEffect(
    () => {
      const setGoal = () => setLastWordGoal(wordCount - (wordCount % wordGoal));
      if (wordCount < lastWordGoal) {
        setGoal();
      }
      if (wordCount > nextWordGoal) {
        setGoal();
        getNewImage(imageSettings);
      }
    },
    [wordCount]
  );

  return (
    <>
      <Container>
        <MainDiv>
          <Splitter
            percentage={true}
            primaryMinSize={25}
            secondaryMinSize={25}
            secondaryInitialSize={secondaryPaneSize}
            onSecondaryPaneSizeChange={setSecondaryPaneSize}
          >
            <SplitView>
              <SectionBar className="level">
                <div className="level-left" />
                <div className="level-right">
                  <div className="level-item">
                    {wordCount}/{nextWordGoal}
                  </div>
                </div>
              </SectionBar>
              <Editor
                placeholder="Start typing..."
                editorState={editorState}
                onChange={updateEditorState}
                handleKeyCommand={handleKeyCommand}
              />
            </SplitView>
            <SplitView>
              <SectionBar className="level">
                <div className="level-left" />
                <div className="level-right">
                  <div className="level-item">
                    <button
                      className={"button is-outlined is-primary"}
                      onClick={() => setAboutModalOpen(true)}
                    >
                      About
                    </button>
                  </div>
                </div>
              </SectionBar>
              <img
                src={imageUrl}
                style={{
                  maxWidth: "100%",
                  maxHeight: "95%",
                  cursor: "pointer"
                }}
                onClick={() => window.open(`https://imgur.com/${image.id}`)}
              />
              {/*<pre>{imageData}</pre>*/}
            </SplitView>
          </Splitter>
        </MainDiv>
        <SettingsBar settings={settings} updateSettings={updateSettings} />
      </Container>
      <AboutModal
        isOpen={aboutModalOpen}
        close={() => setAboutModalOpen(false)}
      />
    </>
  );
}

export default App;

function useEditorState() {
  const [editorState, updateEditorState] = useLocalStorage(
    "writer-editor-state",
    EditorState.createEmpty(),
    {
      serialize(editorState) {
        const content = editorState.getCurrentContent();
        return convertToRaw(content);
      },
      deserialize(rawContentState) {
        const content = convertFromRaw(rawContentState);
        return EditorState.moveFocusToEnd(
          EditorState.createWithContent(content)
        );
      }
    }
  );

  const [wordCount, setWordCount] = useState(0);

  function handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      updateEditorState(newState);
      return "handled";
    }
    return "not-handled";
  }

  useEffect(
    () => {
      if (editorState === null) return;
      const content = editorState.getCurrentContent();
      const plainText = content.getPlainText();
      const words = plainText.match(/\S+/g) || [];
      const count = words.length;
      setWordCount(count);
    },
    [editorState]
  );

  return { editorState, updateEditorState, handleKeyCommand, wordCount };
}
