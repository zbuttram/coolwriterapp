import { getSubredditGallery } from "./imgur";
import { random } from "lodash";

const seenImageIds = new Set();

export async function findImage({ animated = false, ...params } = {}, page) {
  if (page > 100) {
    throw new Error("Imgur API likely recursing forever.");
  }

  let gallery = await getSubredditGallery(params);

  console.log("page", page, "gallery unfiltered", gallery);

  if (gallery.length < 0) {
    throw new Error("Imgur API returned empty result set.");
  }

  gallery = filterImageGallery(gallery, { animated });

  console.log("gallery filtered", gallery);

  if (gallery.length === 0) {
    const nextPage = page ? page + 1 : 1;
    return await findImage({ animated, ...params }, nextPage);
  }

  const image = randomArrayItem(gallery);

  seenImageIds.add(image.id);

  return image;
}

function randomArrayItem(array) {
  return array[random(0, array.length - 1)];
}

function filterImageGallery(gallery, { animated }) {
  return gallery
    .filter(img => !seenImageIds.has(img.id))
    .filter(img => img.animated === animated);
}
