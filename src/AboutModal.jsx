import React from "react";
import classNames from "classnames";
import Modal from "./Modal";

function AboutModal({ isOpen, close }) {
  return (
    <Modal isOpen={isOpen} close={close} title={"About"}>
      <h1 className="title">CoolWriterApp</h1>
      <a
        className="button is-link"
        target={"_blank"}
        href={"http://twitter.com/zbuttram"}
      >
        @zbuttram
      </a>
      <hr />
      <p>
        Written with{" "}
        <a target={"_blank"} href="https://reactjs.org/hooks">
          React Hooks
        </a>
      </p>
      <p>
        Code @ <a href="https://gitlab.com/zbuttram/coolwriterapp">GitLab</a>
      </p>
    </Modal>
  );
}

export default AboutModal;
