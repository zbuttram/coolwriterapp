import ky from "ky";

const api = ky.extend({
  headers: { Authorization: "Client-ID 62fe1837f48d1dd" }
});

export async function getSubredditGallery({
  subreddit,
  sort = "top",
  sortWindow = "week",
  page = ""
} = {}) {
  let url = `https://api.imgur.com/3/gallery/r/${subreddit}/${sort}/${sortWindow}`;

  if (page) {
    url = url + `/${page}`;
  }

  return (await api.get(url).json()).data;
}
